package biz.motioncode.weatherreport;

import java.io.Serializable;

public class ForecastBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String day = "";
	private String date = "";
	private String high = "";
	private String low = "";
	private String conditions = "";
	
	public ForecastBean(){
		
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getLow() {
		return low;
	}

	public void setLow(String low) {
		this.low = low;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}
	
	
}
