package biz.motioncode.weatherreport;

import java.io.Serializable;

/**
 *
 * @author bcgreen
 */
public class WeatherBean implements Serializable {
    
	private static final long serialVersionUID = 1L;
	private String temp="";
    private String conditions="";
    private String updatedAt="";
    private String windDir="";
    private String windSpeed="";
    private String humidity="";
    private String pressure="";
    private String visibility="";
    
    public WeatherBean() {
        
    }
        
    public String getPressure(){
        return this.pressure;
    }
    
    public void setPressure(String value){
        this.pressure = value;
    }
    
    public String getVisibility(){
        return this.visibility;
    }
    
    public void setVisibility(String value){
        this.visibility = value;
    }
    
    public String getWindDir(){
        return this.windDir;
    }
    
    public void setWindDir(String value){
        this.windDir = value;
    }
    
    public String getWindSpeed(){
        return this.windSpeed;
    }
    
    public void setWindSpeed(String value){
        this.windSpeed = value;
    }
    
    public String getHumidity(){
        return this.humidity;
    }
    
    public void setHumidity(String value){
        this.humidity = value;
    }
    
    public String getTemp(){
        return this.temp;
    }
    
    public void setTemp(String value){
        this.temp = value;
    }
    
    public String getConditions(){
        return this.conditions;
    }
    
    public void setConditions(String value){
        this.conditions = value;
    }
    
    public String getUpdatedAt(){
        return this.updatedAt;
    }
    
    public void setUpdatedAt(String value){
        this.updatedAt = value;
    }
}
