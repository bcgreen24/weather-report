package biz.motioncode.weatherreport;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author bcgreen
 */
@WebServlet(name = "GetYahooWeather", urlPatterns = {"/GetYahooWeather"})
public class GetYahooWeather extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Create the WeatherBean than will hold the current weather conditions
	private WeatherBean weather = new WeatherBean();
	
	//This will hold a 5-element array of ForeCast beans
    private ArrayList<ForecastBean> forecastList = new ArrayList<ForecastBean>();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("city", request.getParameter("city"));
        
        //Pass the city name entered in the jsp 'city' text field to the 'getWeather' method;
        //this will populate a WeatherBean and ForecastBean
        getWeather(request.getParameter("city"));
        
        //Pass the weather bean and forecast bean to the jsp via the HTTP request object
        request.setAttribute("weather", weather);
        request.setAttribute("forecastList", forecastList);
        
        //Display the index.jsp page
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public void getWeather(String strCity) throws IOException {
        try {
        	//Build the url string that points to the Yahoo weather web service
            String url = String.format("https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%s\")", strCity);
            
            //URL encode the string by replacing all spaces with '%20'
            String url2 = url.replaceAll(" ", "%20");
            
            //Create an XML document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            //Create the document from our web service url
            Document doc = db.parse(url2);

            //Basics - add temp, conditions, updated date to our weather bean
            NodeList nList = doc.getElementsByTagName("yweather:condition");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element e = (Element) node;
                weather.setTemp(e.getAttribute("temp"));
                weather.setConditions(e.getAttribute("text"));
                weather.setUpdatedAt(e.getAttribute("date"));
            }
            
            //Wind - add wind direction and speed to our weather bean
            NodeList nListWind = doc.getElementsByTagName("yweather:wind");
            for (int i = 0; i < nListWind.getLength(); i++) {
                Node node = nListWind.item(i);
                Element e = (Element) node;
                weather.setWindDir(e.getAttribute("direction"));
                weather.setWindSpeed(e.getAttribute("speed"));
            }
            
            //Atmosphere - add humidity, pressure, visibility to our weather bean
            NodeList nListAtmos = doc.getElementsByTagName("yweather:atmosphere");
            for (int i = 0; i < nListAtmos.getLength(); i++) {
                Node node = nListAtmos.item(i);
                Element e = (Element) node;
                weather.setHumidity(e.getAttribute("humidity"));
                weather.setPressure(e.getAttribute("pressure"));
                weather.setVisibility(e.getAttribute("visibility"));
            }
            
            //Forecast - loop through the first 5 forecast nodes in the XML file, add them to a list of forecast beans
            
            //if the forecast bean list isn't empty, clear it
            if(forecastList != null){
            	forecastList.clear();
            }
            NodeList nListForecast = doc.getElementsByTagName("yweather:forecast");
            for (int i = 0; i < 5; i++) {
                Node node = nListForecast.item(i);
                Element e = (Element) node;
                
                //create a ForecastBean
                ForecastBean forecast = new ForecastBean();
                forecast.setDay(e.getAttribute("day"));
                forecast.setDate(e.getAttribute("date"));
                forecast.setHigh(e.getAttribute("high"));
                forecast.setLow(e.getAttribute("low"));
                forecast.setConditions(e.getAttribute("text"));
                
                //Add the ForecastBean to our list of forecast beans
                forecastList.add(forecast);
            }
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            System.out.println(ex);
        }
    }
}