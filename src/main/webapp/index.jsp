<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Get Weather</title>
<link href="/css/style.css" type="text/css" rel="Stylesheet" />
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/weather.js"></script>
</head>
<body>
	<div id="pagewrapper">
		<header>
			<h2>WEATHER SEARCH</h2>
		</header>
		<div id="body">
			<form name="getWeather" id="getWeather" method="get"
				action="GetYahooWeather">
				<span id="form_label"> City: (e.g., 'Merced, CA', 'Tokyo ')</span><br />
				<input type="text" name="city" id="city" /> <input id="submit"
					type="submit" name="submit" value="Get Weather" />
			</form>
			<c:if test="${not empty weather}">
				<div id="currentConditions">
					<h3 id="current_heading">Current Conditions in ${city}</h3>
					<span class="bold">Temperature: </span>${weather.temp}&deg;F<br />
					<span class="bold">Conditions:	</span>${weather.conditions}<br />
					<span class="bold">Humidity: </span>${weather.humidity}%<br />
					<span class="bold">Wind speed: </span>${weather.windSpeed}m/ph<br /> Last updated at:
					${weather.updatedAt}
				</div>
				<div id="forecast">
					<h3 id="forecast_heading">Your 5-day Forecast</h3>
					<c:forEach items="${forecastList}" var="forecast">
						<div class="dayblock">
							<h4 id="dayheading">${forecast.day}</h4>
							${forecast.date}<br />
							High: ${forecast.high}<br />
							Low: ${forecast.low}<br />
							Conditions: ${forecast.conditions}<br />
						</div>
					</c:forEach>
				</div>
				<!-- forecast -->
			</c:if>
		</div>
		<!-- body div -->
		<footer>
			<div id="credits">
				Data courtesy of:<br />
				<div id="wc_logo">
					<img src="/images/weatherchannel.png" width="50" />
				</div>
				<div id="yahoo_logo">
					<img src="/images/yahoo.svg" width="100" />
				</div>
			</div>
		</footer>
	</div>
	<!-- page wrapper -->
</body>
</html>
